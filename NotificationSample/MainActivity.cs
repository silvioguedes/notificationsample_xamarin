﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Java.Lang;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Media;
using Android.Util;
using Android.Content.Res;
using Java.IO;
using System.IO;

namespace NotificationSample
{
    [Activity(Label = "NotificationSample", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private Button startNotification = null;
        private Button startCustomNotification = null;

        private MyNotification notification = null;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main); 
            
            Intent intent = new Intent(this, typeof(PendingIntentActivity));
            notification = new MyNotification(intent,ApplicationContext);

            startNotification = FindViewById<Button>(Resource.Id.startNotification);
            startNotification.Click += delegate
            {
                notification.CreateNotification();
                notification.StartNotification();
            };

            startCustomNotification = FindViewById<Button>(Resource.Id.startCustomNotification);
            startCustomNotification.Click += delegate
            {
                if (notification.IsAndroidVersionJellyBeanOrNewer())
                {
                    notification.CreateCustomNotification();
                    notification.StartCustomNotification();
                }
            };
        }
    }
}

