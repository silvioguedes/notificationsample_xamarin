using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Java.Lang;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Media;
using Android.Util;
using Android.Content.Res;
using Java.IO;
using System.IO;

namespace NotificationSample
{
    class MyNotification
    {
        public const int SIMPLE_NOTIFICATION_ID = 0;
        public const int CUSTOM_NOTIFICATION_ID = 1;
        public const string ACTION_INTENT = "ACTION INTENT";        
        private PendingIntent pendingIntent = null;
        private PendingIntent actionIntent = null;
        private Android.Net.Uri sound = null;
        private int lightsOn = 1000;
        private int lightsOff = 5000;
        private long[] vibratePattern = { 100, 200, 300 };
        private Bitmap bitmap = null;

        private Intent intent = null;
        private Context context = null;
        private Notification notification = null;
        private NotificationManagerCompat notificationManagerCompat = null;
        private NotificationManager notificationManager = null;

        private static Handler mHandler = new Handler();
        public const int TEN_SECONDS = 10000;

        public MyNotification(Intent intent, Context context)
        {
            this.intent = intent;
            this.context = context;
        }

        public bool IsAndroidVersionJellyBeanOrNewer()
        {
            Android.OS.BuildVersionCodes currentApiVersion = Android.OS.Build.VERSION.SdkInt;
            if (currentApiVersion >= Android.OS.BuildVersionCodes.JellyBean)
            {
                return true;
            }

            return false;
        }

        public void CreateNotification()
        {
            if (IsAndroidVersionJellyBeanOrNewer())
            {
                Intent intent = new Intent(context, typeof (PendingIntentActivity));

                Android.App.TaskStackBuilder taskStackBuilder = Android.App.TaskStackBuilder.Create(context);
                taskStackBuilder.AddParentStack(Class.FromType(typeof (PendingIntentActivity)));
                taskStackBuilder.AddNextIntent(intent);

                pendingIntent = taskStackBuilder.GetPendingIntent(SIMPLE_NOTIFICATION_ID,
                    PendingIntentFlags.UpdateCurrent);
                actionIntent = PendingIntent.GetBroadcast(context, 0, new Intent(ACTION_INTENT), 0);
            }

            string soundFile = "sound";
            sound = Android.Net.Uri.Parse("android.resource://" + context.PackageName + "/raw/" + soundFile);

            bitmap = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.large_icon);

            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            inboxStyle.SetBigContentTitle("BIG CONTENT TITLE");
            inboxStyle.SetSummaryText("SUMMARY TEXT");
            inboxStyle.AddLine("LINE 1");
            inboxStyle.AddLine("LINE 2");
            inboxStyle.AddLine("LINE 3");

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.SetSmallIcon(Resource.Drawable.Icon);
            builder.SetContentTitle("CONTENT TITLE");
            builder.SetContentText("CONTENT TEXT");
            builder.SetTicker("TICKER");
            builder.SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis());
            builder.SetAutoCancel(true);
            builder.SetOngoing(false);
            builder.SetLights(Color.Red, lightsOn, lightsOff);
            builder.SetSound(sound);
            builder.SetVibrate(vibratePattern);

            if (IsAndroidVersionJellyBeanOrNewer())
            {
                builder.SetContentIntent(pendingIntent);
                builder.SetDeleteIntent(pendingIntent);
                builder.AddAction(Resource.Drawable.red_icon, "ACTION INTENT", actionIntent);
            }

            builder.SetContentInfo("CONTENT INFO");
            builder.SetNumber(123);
            builder.SetSubText("SUBTEXT");
            builder.SetLargeIcon(bitmap);
            builder.SetStyle(inboxStyle);

            notificationManagerCompat = NotificationManagerCompat.From(context);            
            notification = builder.Build();            
        }

        public void StartNotification()
        {
            notificationManagerCompat.Notify(SIMPLE_NOTIFICATION_ID,notification);
        }

        public void CreateCustomNotification()
        {
            RemoteViews remoteViews = new RemoteViews(context.PackageName, Resource.Layout.NotificationLayout);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.SetSmallIcon(Resource.Drawable.Icon);
            builder.SetContent(remoteViews);

            Intent redIntent = new Intent(context, typeof(RedActivity));
            Intent greenIntent = new Intent(context, typeof(GreenActivity));
            Intent blueIntent = new Intent(context, typeof(BlueActivity));

            Android.Support.V4.App.TaskStackBuilder blueStackBuilder = Android.Support.V4.App.TaskStackBuilder.Create(context);
            blueStackBuilder.AddParentStack(Class.FromType(typeof(BlueActivity)));
            blueStackBuilder.AddNextIntent(blueIntent);
            PendingIntent bluePendingIntent = blueStackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

            Android.Support.V4.App.TaskStackBuilder redStackBuilder = Android.Support.V4.App.TaskStackBuilder.Create(context);
            redStackBuilder.AddParentStack(Class.FromType(typeof(RedActivity)));
            redStackBuilder.AddNextIntent(redIntent);
            PendingIntent redPendingIntent = redStackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

            Android.Support.V4.App.TaskStackBuilder greenStackBuilder = Android.Support.V4.App.TaskStackBuilder.Create(context);
            greenStackBuilder.AddParentStack(Class.FromType(typeof(GreenActivity)));
            greenStackBuilder.AddNextIntent(greenIntent);
            PendingIntent greenPendingIntent = greenStackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

            remoteViews.SetOnClickPendingIntent(Resource.Id.buttonRed, redPendingIntent);
            remoteViews.SetOnClickPendingIntent(Resource.Id.buttonGreen, greenPendingIntent);
            remoteViews.SetOnClickPendingIntent(Resource.Id.buttonBlue, bluePendingIntent);

            notificationManager = (NotificationManager)context.GetSystemService(Context.NotificationService);            
            notification = builder.Build();
        }

        public void StartCustomNotification(){
            notificationManager.Notify(CUSTOM_NOTIFICATION_ID,notification);
        }            
    }
}